# Bomberman-Multiplayer

The prototype is implemented based on Photon PUN 2.
In the first screen the player chooses a room name (empty room name choose a random room) and his player name.
For Photon PUN there is no need for an IP address.

The first player joining a room is automatically the master client. There is no transfer of ownership implemented yet.
If the master client stops the game before end all clients stay in the room but are not updated any more.

The player got a slighty lag compensation to show smoother movement. The lag compensation takes the last two positions in account and lerps between them regarding to a delta time measured on the time difference between the last two sent packets.

To build the game for a platform just go to build settings, choose the platform and build the game. 
The resulting executable is only tested on Mac OS X. The Windows version should work but is not tested at all.

The level objects are spawned at start by the given string pattern. The game is playing in the scene "EmptyLevel". The "Level" is just for planning and iteration purposes.

The scene "Launcher" is the first loaded scene and contains a UI for joining game room and starting the game.

There are prefabs for:
- Floor
- Walls
- Players
- Breakable obstacle
- Unbreakable obstacle
- Bomb

The evaluation and testing of the given frameworks resulted in the choice of PUN, because it is the easiest and fastest possibility to set up the prototype. MLAPI implies a lot of more low level implementation. It is much more flexible for a later implementation if the prototype evolves. But for the purposes of this small prototype PUN is the chioce for iteration speed.

