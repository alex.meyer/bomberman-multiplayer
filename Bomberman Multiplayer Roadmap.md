Bomberman Multiplayer Roadmap

- Research best multiplayer asset (UNET, Photon, MLAPI)
- Create prefabs for breakable and unbreakable obstacles
- Create prefab for base board with walls around built from unbreakable obstacles
- (Create board with breakable and unbreakable obstacles by given patterns)
- Create player character with controller to move orthogonal directions only
- Create bomb prefab with countdown and destroying effect
- Bomb destroys Everything but unbreakable obstacles in star lines by 3 but only one obstacle


Bugs:
X Bombe darf nur 3 Felder lang sein, bzw. nur so lang bis Hindernis!!!
X Bombe darf nur auf dem MasterClient Wirkung zeigen
X Maustaste von Fire1 entfernen
- Eigene gelegte Bombe darf den Leger nicht bremsen bis er sie verlassen hat
X 3./ (4.?) Spieler total kaputt
X Obstacles auch nur auf MasterClient Wirken lassen und zu den Clients kommunizieren.
- Spieler kann durch Bombe rennen
X Nur eine Bombe zur zeit!
X Bug: Player ist nicht zerstoert worden nur auf dem Client
