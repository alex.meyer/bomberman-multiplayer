using UnityEngine;

namespace Data
{
    /*
     * Data holder for global configuration values
     */
    [CreateAssetMenu(fileName = "NewGameConfig", menuName = "Data/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [Header("Bomb")]
        public float maxDamageRange;
        public float bombCountdown;
        public float animationDuration;
        public float timeToHideBombBody;
        public float activateColliderDelay;
        public float bombAbilityCooldown;

        [Header("Damage Receiver")]
        public float delayBeforeShowDamage;

        [Header("Input")]
        public string bombButton;

        [Header("Character")]
        public float moveSpeed;
    }
}