using System;
using UnityEngine;

namespace Data
{
    /*
     * global integer variable
     * By applying the reference to this global variable to controllers creates a connection
     * between objects and provides a notification system
     */
    [CreateAssetMenu(fileName = "NewIntVariable", menuName = "Data/IntVariable")]
    public class IntVariable : ScriptableObject
    {
        [SerializeField] private int initialVal;
        [SerializeField] private int val;

        public int Val
        {
            get => val;
            set
            {
                val = value;
                valueChanged?.Invoke(val);
            }
        }

        public Action<int> valueChanged;

        private void OnEnable()
        {
            val = initialVal;
        }
    }
}