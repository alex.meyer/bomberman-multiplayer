using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
 * Configuration data container
 *
 * Also global dynamic data provider
 *
 * Normally this should be separated.
 * But for prototyping speed it is combined in one class.
 */

namespace Data
{
    [CreateAssetMenu(fileName = "NewPlayerDataContainer", menuName = "Data/PlayerDataContainer")]
    public class PlayerDataContainer : ScriptableObject
    {
        [Header("Static Config")]
        public Color playerColor;
        public int slot;

        [Header("Specific Enum Values")]
        public PlayerStateEnum initialPlayerState;

        [Header("Set Dynamically At Runtime")]
        [SerializeField] private PlayerStateEnum playerState;

        private static List<PlayerDataContainer> _allContainers = new List<PlayerDataContainer>();

        public PlayerStateEnum PlayerState
        {
            get => playerState;
            set
            {
                playerState = value;
                valuesChanged?.Invoke();
            }
        }
        [SerializeField] private string playerName;

        public string PlayerName
        {
            get => playerName;
            set
            {
                playerName = value;
                valuesChanged();
            }
        }
        public Color PlayerNameColor => playerColor;

        public int playerId;

        public Vector3 spawnPoint;

        public Action valuesChanged;

        public bool Free => !playerState.blockSlot;
        public bool Alive => playerState.isAlive;

        public string PlayerStateText => playerState == null ? "" : playerState.text;
        public Color PlayerStateColor => playerState == null ? Color.white : playerState.color;


        private void OnEnable()
        {
            Reset();
        }

        public void Reset()
        {
            playerId = -1;
            playerState = initialPlayerState;
            playerName = "";
        }

        public static PlayerDataContainer GetByIndex(int index)
        {
            if (_allContainers.Count < 1)
            {
                CreateList();
            }

            return _allContainers[index];
        }

        private static void CreateList()
        {
            _allContainers = Resources.LoadAll<PlayerDataContainer>("").ToList();
        }
    }
}