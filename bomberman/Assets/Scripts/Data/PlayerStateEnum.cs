using UnityEngine;

namespace Data
{
    /*
     * Data container for reflecting the player state
     */
    [CreateAssetMenu(fileName = "NewPlayerStateEnum", menuName = "Data/PlayerStateEnum")]
    public class PlayerStateEnum : ScriptableObject
    {
        public string text;
        public Color color;

        public bool blockSlot;
        public bool isAlive;
    }
}
