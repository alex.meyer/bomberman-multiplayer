using System;
using UnityEngine;

namespace Data
{
    /*
     * global variable holding a game state
     * By applying the reference to this global variable to controllers creates a connection
     * between objects and provides a notification system
     */
    [CreateAssetMenu(fileName = "NewGameStateVariable", menuName = "Data/GameStateVariable")]
    public class GameStateVariable : ScriptableObject
    {
        [SerializeField] private GameStateEnum initialState;
        [SerializeField] private GameStateEnum state;

        public GameStateEnum State
        {
            get => state;
            set { state = value;
                gameStateChanged?.Invoke(state);
            }
        }

        public Action<GameStateEnum> gameStateChanged;

        private void OnEnable()
        {
            state = initialState;
        }
    }
}

