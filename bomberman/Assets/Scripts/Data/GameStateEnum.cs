using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    /*
     * Data Container for the possible game states
     * This container holds the logic how different game states should act
     * eg. UI elements depending on game state get config in the current game state
     */
    [CreateAssetMenu(fileName = "NewGameState", menuName = "Data/GameState")]
    public class GameStateEnum : ScriptableObject
    {
        // id for network communication
        public int id;

        public bool playCountdown;
        public bool showCountdownHud;
        public bool showFinishedHud;
        public bool showStoppedHud;
        public bool joinRoomAllowed;
        public bool playerControlsAllowed;
        public bool aPlayerHasWon;

        private static readonly Dictionary<int, GameStateEnum> AllGameStates = new Dictionary<int, GameStateEnum>();

        public static GameStateEnum GetGameStateById(int id)
        {
            if (AllGameStates.Count == 0)
            {
                CreateDictionary();
            }

            return !AllGameStates.ContainsKey(id) ? null : AllGameStates[id];
        }

        private static void CreateDictionary()
        {
            if (AllGameStates.Count > 0)
            {
                return;
            }

            var instances = Resources.LoadAll<GameStateEnum>("");

            foreach (var instance in instances)
            {
                AllGameStates[instance.id] = instance;
            }
        }
    }
}