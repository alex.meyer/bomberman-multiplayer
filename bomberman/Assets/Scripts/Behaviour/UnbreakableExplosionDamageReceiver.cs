using Bomb;
using UnityEngine;

namespace Behaviour
{
    /*
     * Unbreakable damage receiver
     *
     * (does nothing, indeed not needed yet. Eventually later to complete the whole implementation for visual feedback)
     */
    public class UnbreakableExplosionDamageReceiver : MonoBehaviour, IExplosionDamageReceiver
    {
        public void ReceiveExplosionDamage(float distance)
        {
            // Do nothing for now, just block the draw
        }
    }
}