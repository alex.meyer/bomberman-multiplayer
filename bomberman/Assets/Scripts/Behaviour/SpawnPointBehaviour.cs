using Data;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Serialization;

namespace Behaviour
{
    /*
     * spawn point controller for defining spawn points in editor
     */
    public class SpawnPointBehaviour : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        [Header("Config")]
        [SerializeField] private PlayerDataContainer playerDataContainer;
        [FormerlySerializedAs("radius")]
        [SerializeField] private float gizmosRadius;

        private void OnDrawGizmos()
        {
            if (playerDataContainer != null)
            {
                Gizmos.color = playerDataContainer.playerColor;
            }

            Gizmos.DrawSphere(transform.position, gizmosRadius);
        }

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            var slot = (int) info.photonView.InstantiationData[0];
            playerDataContainer = PlayerDataContainer.GetByIndex(slot);
            if (playerDataContainer != null)
            {
                playerDataContainer.spawnPoint = transform.position;
            }
        }
    }
}
