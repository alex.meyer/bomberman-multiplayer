using System.Collections;
using Bomb;
using Data;
using Photon.Pun;
using UnityEngine;

namespace Behaviour
{
    /*
     * damage receiver specialised for breakables
     */
    public class BreakableExplosionDamageReceiver  : MonoBehaviour, IExplosionDamageReceiver
    {
        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;

        public void ReceiveExplosionDamage(float distance)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                StartCoroutine(WaitDelayAndDestroy());
            }
        }

        IEnumerator WaitDelayAndDestroy()
        {
            yield return new WaitForSeconds(gameConfig.delayBeforeShowDamage);

            // Todo: Play explosion animation
            PhotonNetwork.Destroy(gameObject);
        }
    }
}