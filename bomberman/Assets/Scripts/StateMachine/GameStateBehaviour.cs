using Data;
using UnityEngine;

namespace StateMachine
{
    /*
     * GameStateBehaviour for the Animator state machine triggering the game states by setting the
     * configured game state enum values to the global variable
     *
     * The state machine ie configured via AnimatorController and organised by parameters
     */
    public class GameStateBehaviour : StateMachineBehaviour
    {
        [SerializeField] private GameStateEnum gameState;
        [SerializeField] private GameStateVariable currentGameState;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            base.OnStateEnter(animator, stateInfo, layerIndex);

            currentGameState.State = gameState;
        }
    }
}

