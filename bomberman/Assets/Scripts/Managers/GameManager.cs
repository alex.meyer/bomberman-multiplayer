using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Character;
using Data;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    /*
     * Game Manager responsible for the game state management, network communication
     * and level builder
     *
     * (could be split in different components, because of time issue not split up)
     */
    public class GameManager : MonoBehaviourPunCallbacks, IOnEventCallback
    {
        // for prototyping just keep the level string here
        // Q = empty
        // O = breakable
        // X = unbreakable

        private const string Level =
            "QQOQQ\n" +
            "QXOXQ\n" +
            "OOOOO\n" +
            "QXOXQ\n" +
            "QQOQQ";


        // Todo: bigger levels work but need camera adjustment (Cinemachine)
        // private const string Level =
        //     "QQQOQQQ\n" +
        //     "QXQOQXQ\n" +
        //     "QQXOXQQ\n" +
        //     "OOOOOOO\n" +
        //     "QQXOXQQ\n" +
        //     "QXQOQXQ\n" +
        //     "QQQOQQQ";


        // keep the numbers for the Photon events in the range [1...199]
        // above are reserved
        // and 0 is abused by photon as wildcard in event caching
        private const byte SpawnPlayerPrefabOnClient = 1;
        private const byte FreePlayerSlot = 2;
        private const byte CountdownSecondTick = 3;
        private const byte GameStateChangedEvent = 4;
        private const byte WonGameEvent = 5;
        public const byte SpawnBomb = 6;

        private static readonly int CountdownActive = Animator.StringToHash("CountdownActive");
        private static readonly int AmountOfAlivePlayers = Animator.StringToHash("AmountOfAlivePlayers");

        public static GameManager Instance;

        [Header("Specific Enum Values")]
        [SerializeField] private PlayerStateEnum notConnectedEnum;

        [Header("Variables")]
        [SerializeField] private GameStateVariable currentGameState;
        [SerializeField] private IntVariable amountOfAlivePlayers;
        [SerializeField] private IntVariable runningCountdown;
        [SerializeField] private IntVariable playerWon;

        [Header("Prefabs")]
        [SerializeField] private GameObject playerPrefab;
        [SerializeField] private GameObject floor; // 10x10
        [SerializeField] private GameObject wall;
        [SerializeField] private GameObject unbreakable;
        [SerializeField] private GameObject breakable;
        [SerializeField] private GameObject bomb;
        [SerializeField] private GameObject spawnPoint;

        [Header("Components")]
        [SerializeField] private List<PlayerDataContainer> playerDataContainers;
        [SerializeField] private Animator gameStateMachineAnimator;

        [SerializeField] private TextMeshProUGUI roomNameText;

        [Header("Static Config")]
        [SerializeField] private float dropInCountdownTime = 60f;

        [Header("Dynamic Values")]
        [SerializeField] private float timeLeft;

        private void Awake()
        {
            ValidateReferences();

            // if this isn't the master client, don't organize the game states
            if (!PhotonNetwork.IsMasterClient)
            {
                gameStateMachineAnimator.enabled = false;
            }

            // set the animator to active countdown
            gameStateMachineAnimator.SetBool(CountdownActive, true);
        }

        public override void OnEnable()
        {
            base.OnEnable();

            RegisterListeners();
            ResetPlayerDataContainers();
            AnalyseAndBuildLevel(Level);
        }

        public override void OnDisable()
        {
            base.OnDisable();

            UnregisterListeners();
            ResetPlayerDataContainers();
        }

        private void ResetPlayerDataContainers()
        {
            foreach (var playerDataContainer in playerDataContainers)
            {
                playerDataContainer.Reset();
            }
        }

        private void Start()
        {
            Instance = this;

            Debug.Log($"[Game MANAGER] started GameManager in room: {PhotonNetwork.CurrentRoom.Name}");

            // we're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
            var count = PhotonNetwork.CurrentRoom.PlayerCount;
            if (count > playerDataContainers.Count)
            {
                Debug.LogError($"[GAME MANAGER]  More than 4 players in this room!");
                LeaveRoom();
            }

            // not master clients will spawn via event
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log("[GAME MANAGER] master client is spawning immediately its prefab...");
                SpawnAndPositionPlayer(GetFirstFreeDataContainer(), PhotonNetwork.LocalPlayer.ActorNumber, PhotonNetwork.NickName);
            }

            roomNameText.text = $"Room: {PhotonNetwork.CurrentRoom.Name}";
        }

        // only execute this on MasterClient
        private PlayerDataContainer GetFirstFreeDataContainer()
        {
            return PhotonNetwork.IsMasterClient ? playerDataContainers.FirstOrDefault(p => p.Free) : null;
        }

        private int GetFirstFreePlayerDataContainerSlot()
        {
            return playerDataContainers.FindIndex(p => p.Free);
        }

        // should be executed on each client
        private void SpawnAndPositionPlayer(PlayerDataContainer playerDataContainer, int playerId, string playerName)
        {
            if (playerDataContainer == null)
            {
                return;
            }

            var count = PhotonNetwork.CurrentRoom.PlayerCount;
            var spawnPosition = playerDataContainer.spawnPoint;

            Debug.Log(
                $"[GAME MANAGER] count: {count}," +
                $" playerDataContainer: {playerDataContainer.name}, " +
                $"spawnPosition: {spawnPosition}, " +
                $"player name: {playerName}");

            Debug.Log($"[GAME MANAGER] Instantiating LocalPlayer from {SceneManager.GetActiveScene().name}");

            var customData = new object[] {playerDataContainer.slot, playerId, playerName};
            PhotonNetwork.Instantiate(playerPrefab.name, spawnPosition, Quaternion.identity, 0, customData);
        }

        /// <summary>
        /// Called when the local player left the room. We need to load the launcher scene.
        /// </summary>
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player other)
        {
            Debug.Log($"[GAME MANAGER] OnPlayerEnteredRoom() {other.NickName}"); // not seen if you're the player connecting

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log($"[GAME MANAGER] OnPlayerEnteredRoom IsMasterClient {PhotonNetwork.IsMasterClient}");
            }

            if (!currentGameState.State.joinRoomAllowed)
            {
                // Todo: Show UI for running game... but allow to watch the game
                return;
            }

            // let the client instantiate its player prefab with correct slot number.
            var firstFreeSlot = GetFirstFreePlayerDataContainerSlot();
            if (firstFreeSlot < 0)
            {
                return;
            }

            Debug.Log($"[GAME MANAGER] raise spawn player event for player: {other.ActorNumber} with assigned slot: {firstFreeSlot}");

            var spawnPlayerEventData = new object[] {other.ActorNumber, firstFreeSlot, other.NickName};
            // only clients should receive this event
            var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
            PhotonNetwork.RaiseEvent(SpawnPlayerPrefabOnClient, spawnPlayerEventData, raiseEventOptions, SendOptions.SendReliable);
        }

        public override void OnPlayerLeftRoom(Player other)
        {
            Debug.Log($"[GAME MANAGER] OnPlayerLeftRoom() {other.NickName}"); // seen when other disconnects

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.Log($"[GAME MANAGER] OnPlayerLeftRoom IsMasterClient {PhotonNetwork.IsMasterClient}");
            }

            // inform all clients that the slot is free again.
            var freeSlotData = new object[] {other.ActorNumber};
            // all should receive this event
            var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
            PhotonNetwork.RaiseEvent(FreePlayerSlot, freeSlotData, raiseEventOptions, SendOptions.SendReliable);
        }

        // event receiver on all clients
        public void OnEvent(EventData photonEvent)
        {
            // event handler for informing the GameManagers of all clients
            var eventCode = photonEvent.Code;

            switch (eventCode)
            {
                case SpawnPlayerPrefabOnClient:
                    SpawnPlayerPrefabOnClientHandler(photonEvent);
                    break;

                case FreePlayerSlot:
                    FreePlayerSlotHandler(photonEvent);
                    break;

                case CountdownSecondTick:
                    CountdownSecondsTickHandler(photonEvent);
                    break;

                case GameStateChangedEvent:
                    GameStateChangedEventHandler(photonEvent);
                    break;

                case WonGameEvent:
                    WonGameEventHandler(photonEvent);
                    break;

                case SpawnBomb:
                    SpawnBombHandler(photonEvent);
                    break;
            }
        }

        private void SpawnBombHandler(EventData photonEvent)
        {
            var data = (object[]) photonEvent.CustomData;
            var spawnPosition = (Vector3)data[0];

            PhotonNetwork.InstantiateRoomObject(bomb.name, spawnPosition, Quaternion.identity, 0);
        }

        // this is only called for clients
        private void WonGameEventHandler(EventData photonEvent)
        {
            var data = (object[]) photonEvent.CustomData;

            // this is enough. listeners can register to this and GameManagers methods are using it
            playerWon.Val = (int)data[0];

            Debug.Log($"[GAME MANAGER] Received on client: player won: {playerWon.Val}");
        }

        // this is ony called for clients
        private void GameStateChangedEventHandler(EventData photonEvent)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogError($"[GAME MANAGER] game state changed arrived... but this is master client. Don't set game state again");
                return;
            }

            var data = (object[]) photonEvent.CustomData;
            if (data == null)
            {
                Debug.LogError($"[GAME MANAGER] game state changed arrived... but data is null");
                return;
            }

            var id = (int) data[0];
            var newGameState = GameStateEnum.GetGameStateById(id);
            if (newGameState == null)
            {
                Debug.Log($"[GAME MANAGER] game state changed arrived... but no valid state id: {id}");
                return;
            }

            Debug.Log($"[GAME MANAGER] game state changed event arrived... new valid state: {newGameState.name}, id: {id}");

            // this informs all listeners...
            // GameManager also but cares about to not execute the logic on clients again
            currentGameState.State = newGameState;
        }

        private void CountdownSecondsTickHandler(EventData photonEvent)
        {
            var data = (object[]) photonEvent.CustomData;
            if (data == null)
            {
                return;
            }

            var seconds = (int) data[0];

            Debug.Log($"[GAME MANAGER] received seconds tick: {seconds}");

            // just set the value on client side. all views will be informed
            runningCountdown.Val = seconds;
        }

        // executed on client only
        private void SpawnPlayerPrefabOnClientHandler(EventData photonEvent)
        {
            var data = (object[]) photonEvent.CustomData;
            if (data == null)
            {
                return;
            }

            var localPlayerId = PhotonNetwork.LocalPlayer.ActorNumber;
            var playerId = (int) data[0];
            var slot = (int) data[1];
            var playerName = (string) data[2];

            Debug.Log($"[GAME MANAGER] got spawn player event for: {playerId}" +
                      $" on slot {slot}," +
                      $" local player id: {localPlayerId}," +
                      $" nick name: {playerName}");

            if (playerId != localPlayerId || SimpleCharacterController.localPlayerInstance != null)
            {
                Debug.LogWarning("[GAME MANAGER] not the correct client or local client already has spawned game object... don't spawn");
                return;
            }

            Debug.Log("[GAME MANAGER] spawn player on client...");

            var playerDataContainer = playerDataContainers[slot];
            SpawnAndPositionPlayer(playerDataContainer, playerId, playerName);
        }

        private void FreePlayerSlotHandler(EventData photonEvent)
        {
            // just set the PlayerDataContainer to not connected and assign -1 as playerId
            var playerId = (int)((object[])photonEvent.CustomData)[0];
            var playerDataContainer = playerDataContainers.FirstOrDefault(p => p.playerId == playerId);
            if (playerDataContainer == null)
            {
                return;
            }

            Debug.Log($"[GAME MANAGER] free slot: {playerDataContainer.slot}");

            playerDataContainer.playerId = 1;
            playerDataContainer.PlayerState = notConnectedEnum;
        }

        // simple public accessor
        public PlayerDataContainer GetPlayerDataContainer(int slot)
        {
            return slot >= playerDataContainers.Count ? null : playerDataContainers[slot];
        }

        // executed on all clients but gameStateMachineAnimator only active on MasterClient
        private void AmountOfAlivePlayersChanged(int newAmount)
        {
            Debug.Log("[GAME MANAGER] Set the amount of players into the animator");

            gameStateMachineAnimator.SetInteger(AmountOfAlivePlayers, amountOfAlivePlayers.Val);
        }

        // execute on MasterClient only
        private IEnumerator CountdownCoroutine()
        {
            timeLeft = dropInCountdownTime;

            runningCountdown.Val = (int) timeLeft;

            while (timeLeft >= 0)
            {
                yield return null;
                var newTimeLeft = timeLeft - Time.deltaTime;

                if (newTimeLeft < (int) timeLeft)
                {
                    Debug.Log($"[GAME MANAGER] countdown: new: {newTimeLeft} : old: {timeLeft}");

                    runningCountdown.Val = (int) timeLeft;

                    // send event to clients to tick seconds
                    var seconds = new object[] {runningCountdown.Val};
                    // others only should receive this event
                    var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
                    PhotonNetwork.RaiseEvent(CountdownSecondTick, seconds, raiseEventOptions, SendOptions.SendReliable);
                }

                timeLeft = newTimeLeft;
            }

            // normally this should be done via an GameEvent but development speed is priority now...
            gameStateMachineAnimator.SetBool(CountdownActive, false);
        }

        // execute on MasterClient only
        private void GameStateChanged(GameStateEnum newState)
        {
            // only master client may update all other clients to stay synced.
            if (!PhotonNetwork.IsMasterClient ||
                newState == null)
            {
                return;
            }

            if (newState.playCountdown)
            {
                StartCoroutine(CountdownCoroutine());
            }
            else
            {
                StopCoroutine(CountdownCoroutine());
            }

            if (newState.aPlayerHasWon)
            {
                HandlePlayerWon();
            }

            Debug.Log(
                $"[GAME MANAGER] Throw event to inform clients about new game" +
                $" state: {newState.name}," +
                $" id: {newState.id}");

            // send event to clients to set game state
            var gameStateIdEventData = new object[] {newState.id};
            // others only should receive this event
            var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others, CachingOption = EventCaching.AddToRoomCache};
            PhotonNetwork.RaiseEvent(GameStateChangedEvent, gameStateIdEventData, raiseEventOptions, SendOptions.SendReliable);
        }

        // executed on MasterClient only
        private void HandlePlayerWon()
        {
            // search the one who is still alive
            var players = playerDataContainers.FindAll(p => p.PlayerState.isAlive);
            if (players.Count != 1)
            {
                Debug.LogError("[GAME MANAGER] The game logic is wrong... amount of alive players has to be 1");
                return;
            }

            playerWon.Val = players[0].playerId;

            Debug.Log($"[GAME MANAGER] Handle Player Won... Throw event with won player id: {playerWon.Val}");

            // send event to clients to set game state
            var gameStateIdEventData = new object[] {playerWon.Val};
            // others only should receive this event
            var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others};
            PhotonNetwork.RaiseEvent(WonGameEvent, gameStateIdEventData, raiseEventOptions, SendOptions.SendReliable);
        }

        public string GetPlayerWonName()
        {
            var playerDataContainer = playerDataContainers.FirstOrDefault(p => p.playerId == playerWon.Val);
            return playerDataContainer != null ? playerDataContainer.PlayerName : "";
        }

        // instantiate all observed objects for all players
        private void AnalyseAndBuildLevel(string level)
        {
            var linesArray = level.Split('\n');
            var yDimension = linesArray.Length;
            var xDimension = linesArray[0].Length;
            // now we know the dimensions, build the floor by spawning it synced
            var floorGo = PhotonNetwork.InstantiateRoomObject(floor.name, Vector3.zero, Quaternion.identity);
            floorGo.transform.localScale = new Vector3(xDimension / 10f, 1f, yDimension / 10f);
            // Place 4 walls around the floor
            // right
            InstantiateWall(new Vector3(xDimension * 0.5f, 0.5f, 0), Quaternion.identity, new Vector3(0.1f, 1, yDimension));
            // left
            InstantiateWall(new Vector3(-xDimension * 0.5f, 0.5f, 0), Quaternion.identity, new Vector3(0.1f, 1, yDimension));
            // top
            InstantiateWall(new Vector3(0, 0.5f, yDimension * 0.5f), Quaternion.Euler(0, 90, 0), new Vector3(0.1f, 1, xDimension));
            // bottom
            InstantiateWall(new Vector3(0, 0.5f, -yDimension * 0.5f), Quaternion.Euler(0, 90, 0), new Vector3(0.1f, 1, xDimension));

            var pos = Vector3.zero;
            pos.z = 0 - (int)(yDimension * 0.5f);
            pos.y = 0.5f;
            for(var lineIndex = 0; lineIndex < linesArray.Length; lineIndex++)
            {
                var line = linesArray[lineIndex];
                pos.x = 0 - (int)(xDimension * 0.5f);
                for (var colIndex = 0; colIndex < line.Length; colIndex++)
                {
                    var chr = line[colIndex];
                    switch (chr)
                    {
                        case 'O':
                            PhotonNetwork.InstantiateRoomObject(breakable.name, pos, Quaternion.identity);
                            break;
                        case 'X':
                            PhotonNetwork.InstantiateRoomObject(unbreakable.name, pos, Quaternion.identity);
                            break;
                    }

                    // if one of the edges
                    if ((colIndex == 0 || colIndex == line.Length - 1) &&
                        (lineIndex == 0 || lineIndex == line.Length - 1))
                    {
                        var slotNum = colIndex == 0
                            ? lineIndex == 0 ? 0 : 2
                            : lineIndex == 0 ? 1 : 3;

                        var customData = new object[] {slotNum};
                        PhotonNetwork.InstantiateRoomObject(spawnPoint.name, pos, Quaternion.identity, 0, customData);
                    }

                    pos.x += 1f;
                }

                pos.z += 1f;
            }
        }

        private void InstantiateWall(Vector3 pos, Quaternion rotation, Vector3 scale)
        {
            var wallPos = pos;
            var wallGo = PhotonNetwork.InstantiateRoomObject(wall.name, wallPos, rotation);
            wallGo.transform.localScale = scale;
        }

        private void RegisterListeners()
        {
            PhotonNetwork.AddCallbackTarget(this);
            amountOfAlivePlayers.valueChanged += AmountOfAlivePlayersChanged;
            currentGameState.gameStateChanged += GameStateChanged;
        }

        private void UnregisterListeners()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
            amountOfAlivePlayers.valueChanged -= AmountOfAlivePlayersChanged;
            currentGameState.gameStateChanged -= GameStateChanged;
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameState != null, "currentGameState != null");
            Debug.Assert(amountOfAlivePlayers != null, "amountOfAlivePlayers != null");
            Debug.Assert(playerWon != null, "playerWon != null");
            Debug.Assert(runningCountdown != null, "runningCountdown != null");
            Debug.Assert(playerPrefab != null, "[GAME MANAGER] playerPrefab != null");
            Debug.Assert(floor != null, "floor != null");
            Debug.Assert(wall != null, "wall != null");
            Debug.Assert(breakable != null, "breakable != null");
            Debug.Assert(unbreakable != null, "unbreakable != null");
            Debug.Assert(bomb != null, "bomb != null");
            Debug.Assert(spawnPoint != null, "spawnPoint != null");
            Debug.Assert(playerDataContainers.Count == 4, "[GAME MANAGER] playerDataContainers.Count == 4");
            Debug.Assert(roomNameText != null, "[GAME MANAGER] roomNameText != null");
            Debug.Assert(gameStateMachineAnimator != null, "gameStateMachineAnimator != null");
        }
    }
}
