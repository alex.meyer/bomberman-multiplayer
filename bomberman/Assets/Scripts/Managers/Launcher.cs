using Photon.Pun;
using Photon.Realtime;
using UI;
using UnityEngine;

namespace Managers
{
    /*
     * Photon related game launcher with room and player name input
     */
    public class Launcher : MonoBehaviourPunCallbacks
    {
        public const string LevelName = "EmptyLevel";

        [Header("Components")]
        [SerializeField] private GameObject controlPanel;
        [SerializeField] private GameObject progressLabel;
        [SerializeField] private RoomNameInputField roomNameInputField;

        [Header("Meta Data")]
        [SerializeField] private byte maxPlayersPerRoom = 4;
        [SerializeField] private string roomName;

        [Header("Dynamic Values")]
        [SerializeField] private bool isConnecting;

        private string gameVersion = "1";
        void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public override void OnEnable()
        {
            base.OnEnable();

            ValidateReferences();
            RegisterListeners();
            roomName = roomNameInputField.RoomName;
        }

        public override void OnDisable()
        {
            base.OnDisable();

            UnregisterListeners();
        }

        void Start()
        {
            progressLabel.SetActive(false);
            controlPanel.SetActive(true);
        }

        public void Connect()
        {
            if (PhotonNetwork.IsConnected)
            {
                TryJoinRoom();
            }
            else
            {
                isConnecting = PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = gameVersion;
            }

            progressLabel.SetActive(true);
            controlPanel.SetActive(false);
        }

        public override void OnConnectedToMaster()
        {
            Debug.Log("[LAUNCHER] OnConnectedToMaster() was called by PUN");
            if (!isConnecting)
            {
                return;
            }

            TryJoinRoom();
            isConnecting = false;
        }

        private void TryJoinRoom()
        {
            Debug.Log($"[LAUNCHER] Given room: {roomName}");

            if (string.IsNullOrEmpty(roomName))
            {
                Debug.Log("[LAUNCHER] no room name -> join random room");
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                Debug.Log($"[LAUNCHER] join room: {roomName}");
                PhotonNetwork.JoinRoom(roomName);
            }
        }


        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("[LAUNCHER]  OnDisconnected() was called by PUN with reason {0}", cause);
            progressLabel.SetActive(false);
            controlPanel.SetActive(true);
            isConnecting = false;
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("[LAUNCHER] OnJoinRandomFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

            // null for random room
            PhotonNetwork.CreateRoom(null, new RoomOptions{MaxPlayers = maxPlayersPerRoom});
        }

        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log($"[LAUNCHER] OnJoinRoomFailed() was called by PUN. No '{roomName}' room available," +
                      $" so we create one.\nCalling: PhotonNetwork.CreateRoom {roomName}");

            PhotonNetwork.CreateRoom(roomName, new RoomOptions{MaxPlayers = maxPlayersPerRoom});
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("[LAUNCHER]  OnJoinedRoom() called by PUN. Now this client is in a room.");
            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log($"[LAUNCHER] We load the '{LevelName}' ");
                PhotonNetwork.LoadLevel(LevelName);
            }
        }

        private void RoomNameChanged(string newRoomName)
        {
            Debug.Log($"[LAUNCHER] room name changed to: {roomName}");

            roomName = newRoomName;
        }

        private void RegisterListeners()
        {
            roomNameInputField.roomNameChanged += RoomNameChanged;
        }

        private void UnregisterListeners()
        {
            roomNameInputField.roomNameChanged -= RoomNameChanged;
        }

        private void ValidateReferences()
        {
            Debug.Assert(controlPanel != null, "");
            Debug.Assert(progressLabel != null, "progressLabel != null");
            Debug.Assert(roomNameInputField != null, "roomNameInputField != null");
        }
    }
}

