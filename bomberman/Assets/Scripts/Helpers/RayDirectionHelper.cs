using UnityEngine;

namespace Helpers
{
    /*
     * static helper class to give coded configuration for a enum
     * Todo: This probably should be refactored into a scriptable object instead
     */
    public enum RayDirection
    {
        Forward,
        Backward,
        Right,
        Left,
    }

    public static class RayDirectionHelper
    {
        public static Vector3 DirectionByRayDirection(RayDirection rayDirection)
        {
            switch (rayDirection)
            {
                case RayDirection.Backward:
                    return Vector3.back;
                case RayDirection.Right:
                    return Vector3.right;
                case RayDirection.Left:
                    return Vector3.left;
                default:
                case RayDirection.Forward:
                    return Vector3.forward;
            }
        }

        public static void SetEulerYByRayDirection(this Transform transform, RayDirection rayDirection)
        {
            var euler = transform.eulerAngles;
            switch (rayDirection)
            {
                case RayDirection.Backward:
                    euler.y = 180f;
                    break;
                case RayDirection.Right:
                    euler.y = 90f;
                    break;
                case RayDirection.Left:
                    euler.y = -90f;
                    break;
                default:
                case RayDirection.Forward:
                    euler.y = 0f;
                    break;
            }

            transform.eulerAngles = euler;
        }
    }
}