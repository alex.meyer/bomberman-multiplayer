using System.Collections;
using Bomb;
using Data;
using UnityEngine;

namespace Character
{
    public class CharacterExplosionDamageReceiver : MonoBehaviour, IExplosionDamageReceiver
    {
        /*
         * damage receiver for characters
         *
         */
        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;

        public void ReceiveExplosionDamage(float distance)
        {
            StartCoroutine(WaitDelayAndDestroy());
        }

        IEnumerator WaitDelayAndDestroy()
        {
            yield return new WaitForSeconds(gameConfig.delayBeforeShowDamage);
            // Just set inactive, because player should watch the rest of the match
            // and can leave room by clicking leave button
            // also the disabling will set the player status in SimpleCharacterController
            gameObject.SetActive(false);
            transform.position = Vector3.right * 1000;
        }
    }
}
