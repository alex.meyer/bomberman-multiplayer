using Photon.Pun;
using UnityEngine;

// This class only compensates a lag
// To predict the current position we would have to calculate the current position
// by assuming the player would move in the last direction and continues to turn by last turned angle

namespace Character
{
    public class LagCompensationTransformView : MonoBehaviourPun, IPunObservable
    {
        //Values that will be synced over network
        Vector3 latestPos;
        Quaternion latestRot;
        //Lag compensation
        float currentTime = 0;
        double currentPacketTime = 0;
        double lastPacketTime = 0;
        Vector3 positionAtLastPacket = Vector3.zero;
        Quaternion rotationAtLastPacket = Quaternion.identity;

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                //We own this player: send the others our data
                stream.SendNext(transform.position);
                stream.SendNext(transform.rotation);
            }
            else
            {
                //Network player, receive data
                latestPos = (Vector3)stream.ReceiveNext();
                latestRot = (Quaternion)stream.ReceiveNext();

                //Lag compensation:
                // reset current time value
                currentTime = 0.0f;
                // switch old current to last packet time
                lastPacketTime = currentPacketTime;
                // set actual one to sent server time
                currentPacketTime = info.SentServerTime;
                // and store actual position and rotation
                positionAtLastPacket = transform.position;
                rotationAtLastPacket = transform.rotation;
            }
        }

        // Update is called once per frame
        private void Update()
        {
            // only compensate if this is not the current player on this client
            if (photonView.IsMine)
            {
                return;
            }

            //Lag compensation:
            // calc the diff between last and current snapshot
            var timeToReachGoal = currentPacketTime - lastPacketTime;
            // iterate the time value since last snapshot
            currentTime += Time.deltaTime;

            //Update remote player by lerping between the two positions by calculated time value normalised by the time delta of snapshots
            transform.position = Vector3.Lerp(positionAtLastPacket, latestPos, (float)(currentTime / timeToReachGoal));
            transform.rotation = Quaternion.Lerp(rotationAtLastPacket, latestRot, (float)(currentTime / timeToReachGoal));
        }
    }
}