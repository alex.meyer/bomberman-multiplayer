using Data;
using Managers;
using Photon.Pun;
using UnityEngine;

namespace Character
{
    /*
     * CharacterController for the player prefab
     * simple input to movement controller
     * assignment to slot via data container
     * implements photon instantiation callback
     */

    public class SimpleCharacterController : MonoBehaviourPun, IPunInstantiateMagicCallback
    {
        // To speed up the prototyping I don't separate the player input and the character movement for now
        // later the input should be outsourced to an InputHandler class
        // also the lifetime logic could be moved to another component

        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;
        [SerializeField] private PlayerDataContainer playerDataContainer;

        [Header("Specific Enum Values")]
        [SerializeField] private PlayerStateEnum deadStateEnum;
        [SerializeField] private PlayerStateEnum aliveStateEnum;

        [Header("Variables")]
        [SerializeField] private GameStateVariable currentGameState;
        [SerializeField] private IntVariable amountOfAlivePlayer;

        public static GameObject localPlayerInstance;

        public PlayerDataContainer PlayerDataContainer
        {
            get => playerDataContainer;
            set
            {
                playerDataContainer = value;
                Init();
            }
        }

        [Header("Components")]
        [SerializeField] private CharacterController charController;
        [SerializeField] private Renderer playerRenderer;

        private void Awake()
        {
            if (photonView.IsMine)
            {
                localPlayerInstance = gameObject;
            }
        }

        private void OnEnable()
        {
            ValidateReferences();
        }

        private void OnDisable()
        {
            Debug.Log("[PLAYER] set state to dead in OnDisable()");

            playerDataContainer.PlayerState = deadStateEnum;
            amountOfAlivePlayer.Val -= 1;
        }

        private void Update()
        {
            ControlPlayerCharacter();
        }

        private void Init()
        {
            Debug.Log("[PLAYER] Init player...");

            // set color of the player
            playerRenderer.material.color = playerDataContainer.playerColor;
            playerDataContainer.PlayerState = aliveStateEnum;
            amountOfAlivePlayer.Val += 1;

            Debug.Log(
                $"[PLAYER] initialised with data container: {playerDataContainer.name}," +
                $" color: {playerDataContainer.playerColor}" +
                $" and status: {playerDataContainer.PlayerState.name}");
        }

        private void ControlPlayerCharacter()
        {
            if (currentGameState.State == null ||
                !currentGameState.State.playerControlsAllowed)
            {
                return;
            }

            if (!photonView.IsMine && PhotonNetwork.IsConnected)
            {
                return;
            }

            var direction = GetMoveDirectionFromInput();
            if (direction.magnitude < 0.1f)
            {
                return;
            }

            var moveVec =  gameConfig.moveSpeed * Time.deltaTime * direction;

            Debug.Log($"[CHAR] move dir: [{direction}], moveVec: [{moveVec}], speed:[{gameConfig.moveSpeed}], delta: [{Time.deltaTime}]");

            // rotate nose into move direction
            charController.transform.forward = direction;
            // move character in move direction
            charController.SimpleMove(moveVec);
        }

        private Vector3 GetMoveDirectionFromInput()
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            return (Vector3.right * horizontal + Vector3.forward * vertical).normalized;
        }

        // custom initialisation provided by photon callback
        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            var slot = (int)info.photonView.InstantiationData[0];
            var playerId = (int)info.photonView.InstantiationData[1];
            var playerName = (string) info.photonView.InstantiationData[2];

            Debug.Log(
                $"[PLAYER] got instantiation callback with slot {slot}, " +
                $"assign player data container to this player id: {playerId}," +
                $"assign player name: {playerName}...");

            // init the player with color and data connections
            PlayerDataContainer = GameManager.Instance.GetPlayerDataContainer(slot);
            PlayerDataContainer.playerId = playerId;
            PlayerDataContainer.PlayerName = playerName;
            gameObject.name += $"[{playerId}][{playerName}]";
        }

        private void ValidateReferences()
        {
            if (charController == null)
            {
                charController = GetComponent<CharacterController>();
            }

            if (playerRenderer != null)
            {
                playerRenderer = GetComponent<Renderer>();
            }

            Debug.Assert(playerRenderer != null, "playerRenderer != null");
            Debug.Assert(charController != null, "charController != null");
            Debug.Assert(currentGameState != null, "currentGameState != null");
            Debug.Assert(amountOfAlivePlayer != null, "amountOfAlivePlayer != null");
        }
    }
}