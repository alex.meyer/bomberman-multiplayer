using System.Collections;
using Data;
using UnityEngine;

namespace Bomb
{
    /*
     * Activates collider after configured delay
     */
    public class ActivateColliderAfterDelayController : MonoBehaviour
    {
        private const string PlayerTag = "Player";

        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;

        [Header("Components")]
        [SerializeField] private Collider objectCollider;

        private void OnEnable()
        {
            ValidateReferences();
            StartCoroutine(ActivateColliderAfterDelay());
        }

        private IEnumerator ActivateColliderAfterDelay()
        {
            objectCollider.enabled = false;

            yield return new WaitForSeconds(gameConfig.activateColliderDelay);

            objectCollider.enabled = true;
        }

        private void ValidateReferences()
        {
            if (objectCollider == null)
            {
                objectCollider = GetComponent<Collider>();
            }

            Debug.Assert(gameConfig != null, "gameConfig != null");
            Debug.Assert(objectCollider != null, "objectCollider != null");
        }
    }
}