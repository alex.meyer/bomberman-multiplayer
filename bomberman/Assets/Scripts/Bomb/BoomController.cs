using System.Collections;
using System.Collections.Generic;
using CommonControllers;
using Data;
using Helpers;
using Photon.Pun;
using TMPro;
using UnityEngine;

namespace Bomb
{
    /*
     * Boom controller is component for exploding entities
     */
    public class BoomController : MonoBehaviourPun
    {
        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;

        [Header("Components")]
        [SerializeField] private Renderer bombRenderer;
        [SerializeField] private TextMeshPro numberText;
        [SerializeField] private CountdownTickController countdownTickController;
        [SerializeField] private ExplosionRayView[] explosionRayViews;

        private readonly Dictionary<RayDirection, ExplosionRayView> _explosionRayViewByRayDirection = new Dictionary<RayDirection, ExplosionRayView>();

        [Header("Dynamic Meta Data")]
        [SerializeField] private int playerId;

        private void OnEnable()
        {
            ValidateReferences();
            RegisterListeners();
            countdownTickController.TimeInSeconds = gameConfig.bombCountdown;
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void CountdownDone()
        {
            // Todo: Particle Effect
            StartCoroutine(PlayExplosionAndDestroyObject());
        }

        private IEnumerator PlayExplosionAndDestroyObject()
        {
            RaycastObjects();

            yield return new WaitForSeconds(gameConfig.timeToHideBombBody);

            bombRenderer.enabled = false;
            numberText.gameObject.SetActive(false);

            yield return new WaitForSeconds(gameConfig.animationDuration - gameConfig.timeToHideBombBody);

            // remove this game object
            PhotonNetwork.Destroy(gameObject);
        }

        private void RaycastObject(Vector3 start, RayDirection rayDirection)
        {
            var direction = RayDirectionHelper.DirectionByRayDirection(rayDirection);
            var distance = gameConfig.maxDamageRange;
            if (Physics.Raycast(start, direction, out var hit, gameConfig.maxDamageRange, Physics.DefaultRaycastLayers))
            {
                var receiver = hit.transform.GetComponent<IExplosionDamageReceiver>();
                receiver?.ReceiveExplosionDamage(hit.distance);
                distance = hit.distance;
            }
            // play explosion animation with found distance
            if (_explosionRayViewByRayDirection.ContainsKey(rayDirection))
            {
                _explosionRayViewByRayDirection[rayDirection].PlayAnimation(distance);
            }
        }

        private void RaycastObjects()
        {
            var pos = transform.position;
            RaycastObject(pos, RayDirection.Forward);
            RaycastObject(pos, RayDirection.Backward);
            RaycastObject(pos, RayDirection.Right);
            RaycastObject(pos, RayDirection.Left);
        }

        private void RegisterListeners()
        {
            countdownTickController.countdownDone += CountdownDone;
        }

        private void UnregisterListeners()
        {
            countdownTickController.countdownDone -= CountdownDone;
        }

        private void ValidateReferences()
        {
            if (countdownTickController == null)
            {
                GetComponent<CountdownTickController>();
            }

            if (bombRenderer == null)
            {
                bombRenderer = GetComponent<Renderer>();
            }

            if (numberText == null)
            {
                numberText = GetComponentInChildren<TextMeshPro>();
            }

            explosionRayViews = GetComponentsInChildren<ExplosionRayView>();
            // build dictionary for faster access
            foreach (var explosionRayView in explosionRayViews)
            {
                _explosionRayViewByRayDirection[explosionRayView.RayDirection] = explosionRayView;
            }

            Debug.Assert(gameConfig != null, "gameConfig != null");
            Debug.Assert(countdownTickController != null, "_countdownTickController != null");
            Debug.Assert(bombRenderer != null, "bombRenderer != null");
            Debug.Assert(numberText != null, "numberText != null");
        }
    }
}