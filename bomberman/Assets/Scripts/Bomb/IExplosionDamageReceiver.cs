namespace Bomb
{
    /*
     * By implementing this interface game objects components are able to receive damage by distance
     */
    public interface IExplosionDamageReceiver
    {
        void ReceiveExplosionDamage(float distance);
    }
}