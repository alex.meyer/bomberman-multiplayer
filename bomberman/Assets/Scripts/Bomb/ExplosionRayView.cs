using Helpers;
using UnityEngine;

namespace Bomb
{
    /*
     * View controller for a single explosion ray
     */
    public class ExplosionRayView : MonoBehaviour
    {
        private static readonly int Blow1Hash = Animator.StringToHash("Blow 1");
        private static readonly int Blow2Hash = Animator.StringToHash("Blow 2");
        private static readonly int Blow3Hash = Animator.StringToHash("Blow 3");

        [SerializeField] private Animator animator;
        [SerializeField] private RayDirection rayDirection;
        public RayDirection RayDirection => rayDirection;
        [SerializeField] private float maxDistanceForBlow1;
        [SerializeField] private float maxDistanceForBlow2;
        [SerializeField] private float maxDistanceForBlow3;
        public float MaxDistanceForBlow3 => maxDistanceForBlow3;

        private void OnEnable()
        {
            ValidateReferences();
            Init();
        }

        private void Init()
        {
            transform.SetEulerYByRayDirection(rayDirection);
        }

        public void PlayAnimation(float distance)
        {
            if (distance < maxDistanceForBlow1)
            {
                animator.SetTrigger(Blow1Hash);
            }
            else if (distance < maxDistanceForBlow2)
            {
                animator.SetTrigger(Blow2Hash);
            }
            else
            {
                animator.SetTrigger(Blow3Hash);
            }
        }

        private void ValidateReferences()
        {
            if (animator == null)
            {
                animator = GetComponent<Animator>();
            }

            Debug.Assert(animator != null, "animator != null");
        }
    }
}