using System.Collections;
using Data;
using ExitGames.Client.Photon;
using Managers;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace Bomb
{
    /*
     * Bomb ability component sits on entities which can spawn bombs
     */

    public class BombAbility : MonoBehaviourPun
    {
        [Header("Config")]
        [SerializeField] private GameConfig gameConfig;
        // To keep things easy the input is not extracted to a handler class
        [Header("Variables")]
        [SerializeField] private GameStateVariable currentGameState;

        [Header("Prefabs")]
        [SerializeField] private GameObject bombPrefab;

        [Header("Dynamic Meta Data")]
        [SerializeField] private float cooldown;

        private void OnEnable()
        {
            ValidateReferences();
            cooldown = 0f;
        }

        private void Update()
        {
            SpawnBombOnRoundedCoordsIfFire1Input();
        }

        private void SpawnBombOnRoundedCoordsIfFire1Input()
        {
            if (currentGameState.State == null ||
                !currentGameState.State.playerControlsAllowed ||
                cooldown > 0f)
            {
                return;
            }

            // if this is not the current players instance just ignore the method
            if (!photonView.IsMine && PhotonNetwork.IsConnected)
            {
                return;
            }

            if (!Input.GetButtonDown(gameConfig.bombButton))
            {
                return;
            }

            var spawnPosition = transform.position;
            spawnPosition.x = Mathf.Round(spawnPosition.x);
            spawnPosition.z = Mathf.Round(spawnPosition.z);

            // throw event to let master client spawn the bomb for all
            var content = new object[] { spawnPosition};
            var raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
            PhotonNetwork.RaiseEvent(GameManager.SpawnBomb, content, raiseEventOptions, SendOptions.SendReliable);

            StartCoroutine(CooldownCoroutine());
        }

        private IEnumerator CooldownCoroutine()
        {
            cooldown = gameConfig.bombAbilityCooldown;

            while (cooldown > 0f)
            {
                yield return null;

                cooldown -= Time.deltaTime;
            }
        }

        private void ValidateReferences()
        {
            Debug.Assert(gameConfig != null, "gameConfig != null");
            Debug.Assert(currentGameState != null, "currentGameState != null");
            Debug.Assert(bombPrefab != null, "bombPrefab != null");
        }
    }
}