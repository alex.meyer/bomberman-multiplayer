using CommonControllers;
using TMPro;
using UnityEngine;

namespace UI
{
    /*
     * Countdown UI component to display a HUD countdown number
     */
    public class CountdownTextOnTop : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private TextMeshPro text;
        [SerializeField] private CountdownTickController countdownTickController;

        private void OnEnable()
        {
            ValidateReferences();
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void UpdateView(float number)
        {
            text.text = $"{(int) number + 1}";
        }

        private void RegisterListeners()
        {
            countdownTickController.getTick += UpdateView;
        }

        private void UnregisterListeners()
        {
            countdownTickController.getTick -= UpdateView;
        }

        private void ValidateReferences()
        {
            if (text == null)
            {
                text = GetComponent<TextMeshPro>();
            }

            if (countdownTickController == null)
            {
                countdownTickController = GetComponentInParent<CountdownTickController>();
            }

            Debug.Assert(text != null, "text != null");
            Debug.Assert(countdownTickController != null, "countdownTickController != null");
        }
    }
}