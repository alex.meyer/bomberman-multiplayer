using Data;
using Managers;
using TMPro;
using UnityEngine;

namespace UI
{
    /*
     * View controller for the finished game HUD
     */

    public class FinishGameController : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateVariable currentGameState;
        [SerializeField] private IntVariable playerWon;

        [Header("Components")]
        [SerializeField] private TextMeshProUGUI playerWonText;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
            GameStateChanged(currentGameState.State);
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void OnEnable()
        {
            UpdateView();
        }

        private void UpdateView()
        {
            var playerName = GameManager.Instance.GetPlayerWonName();
            playerWonText.text = $"{playerName} Won!";
        }

        private void GameStateChanged(GameStateEnum newState)
        {
            if (newState == null)
            {
                Debug.Log("[FINISH HUD] empty game state variable, ignore.");

                gameObject.SetActive(false);
                return;
            }

            Debug.Log(
                $"[FINISH HUD] Game state changed event: current state: {newState.name}" +
                $" should show finish hud {newState.showFinishedHud}");

            gameObject.SetActive(newState.showFinishedHud);
        }

        private void PlayerWonChanged(int val)
        {
            UpdateView();
        }

        private void RegisterLifetimeListeners()
        {
            currentGameState.gameStateChanged += GameStateChanged;
            playerWon.valueChanged += PlayerWonChanged;
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameState.gameStateChanged -= GameStateChanged;
            playerWon.valueChanged -= PlayerWonChanged;
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameState != null, "currentGameState != null");
            Debug.Assert(playerWon != null, "playerWon != null");
            Debug.Assert(playerWonText != null, "playerWonText != null");
        }
    }
}