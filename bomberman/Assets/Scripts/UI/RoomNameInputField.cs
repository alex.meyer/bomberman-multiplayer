using System;
using TMPro;
using UnityEngine;

namespace UI
{
    /*
     * view controller for the room name input field
     * sets the room name for photon and is stored in preferences
     */
    [RequireComponent(typeof(TMP_InputField))]
    public class RoomNameInputField : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputField;

        const string roomNamePrefKey = "RoomName";

        public Action<string> roomNameChanged;

        public string RoomName => inputField.text;

        private void OnEnable()
        {
            ValidateReferences();
        }

        void Start ()
        {
            var defaultName = string.Empty;
            if (!PlayerPrefs.HasKey(roomNamePrefKey))
            {
                return;
            }

            defaultName = PlayerPrefs.GetString(roomNamePrefKey);
            inputField.text = defaultName;
            roomNameChanged?.Invoke(inputField.text);
        }

        public void SetRoomName(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                Debug.LogError("Room Name is null or empty");
                return;
            }

            PlayerPrefs.SetString(roomNamePrefKey,value);

            roomNameChanged?.Invoke(inputField.text);
        }

        private void ValidateReferences()
        {
            Debug.Assert(inputField != null, "inputField != null");
        }
    }
}