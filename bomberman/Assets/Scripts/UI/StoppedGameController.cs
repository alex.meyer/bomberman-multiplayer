using Data;
using UnityEngine;

namespace UI
{
    /*
     * UI view controller for the Hud if game is stopped because of too less players
     */
    public class StoppedGameController : MonoBehaviour
    {
        [Header("Variables")] [SerializeField] private GameStateVariable currentGameState;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
            GameStateChanged(currentGameState.State);
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void GameStateChanged(GameStateEnum newState)
        {
            if (newState == null)
            {
                Debug.Log("[FINISH HUD] empty game state variable, ignore.");

                gameObject.SetActive(false);
                return;
            }

            Debug.Log(
                $"[FINISH HUD] Game state changed event: current state: {newState.name}" +
                $" should show stopped hud {newState.showStoppedHud}");

            gameObject.SetActive(newState.showStoppedHud);
        }

        private void RegisterLifetimeListeners()
        {
            currentGameState.gameStateChanged += GameStateChanged;
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameState.gameStateChanged -= GameStateChanged;
        }

        private void ValidateReferences()
        {
            Debug.Assert(currentGameState != null, "currentGameState != null");
        }
    }
}