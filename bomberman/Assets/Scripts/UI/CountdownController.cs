using Data;
using TMPro;
using UnityEngine;

namespace UI
{
    /*
     * Countdown controller for the game start depending n game state
     */
    public class CountdownController : MonoBehaviour
    {
        [Header("Variables")]
        [SerializeField] private GameStateVariable currentGameState;
        [SerializeField] private IntVariable runningCountdown;

        [Header("Components")]
        [SerializeField] private TextMeshProUGUI numberText;

        private void Awake()
        {
            ValidateReferences();
            RegisterLifetimeListeners();
            GameStateChanged(currentGameState.State);
        }

        private void OnDestroy()
        {
            UnregisterLifetimeListeners();
        }

        private void UpdateView()
        {
            numberText.text = $"{runningCountdown.Val}";
        }

        private void GameStateChanged(GameStateEnum newState)
        {
            if (newState == null)
            {
                return;
            }

            gameObject.SetActive(newState.showCountdownHud);
        }

        private void CountdownTick(int val)
        {
            UpdateView();
        }

        private void RegisterLifetimeListeners()
        {
            currentGameState.gameStateChanged += GameStateChanged;
            runningCountdown.valueChanged += CountdownTick;
        }

        private void UnregisterLifetimeListeners()
        {
            currentGameState.gameStateChanged -= GameStateChanged;
            runningCountdown.valueChanged -= CountdownTick;
        }

        private void ValidateReferences()
        {
            Debug.Assert(runningCountdown != null, "runningCountdown != null");
            Debug.Assert(currentGameState != null, "currentGameState != null");
            Debug.Assert(numberText != null, "numberText != null");
        }
    }
}
