using Data;
using TMPro;
using UnityEngine;

namespace UI
{
    /*
     * Player Hud View Controller displays the state of assigned player data
     */
    public class PlayerHudController : MonoBehaviour
    {
        [SerializeField] private PlayerDataContainer playerDataContainer;
        [SerializeField] private TextMeshProUGUI playerNameText;
        [SerializeField] private TextMeshProUGUI playerStateText;

        private void OnEnable()
        {
            ValidateReferences();
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void UpdateView()
        {
            playerNameText.text = playerDataContainer.PlayerName;
            playerNameText.color = playerDataContainer.PlayerNameColor;
            playerStateText.text = playerDataContainer.PlayerStateText;
            playerStateText.color = playerDataContainer.PlayerStateColor;
        }

        private void RegisterListeners()
        {
            playerDataContainer.valuesChanged += UpdateView;
        }

        private void UnregisterListeners()
        {
            playerDataContainer.valuesChanged -= UpdateView;
        }

        private void ValidateReferences()
        {
            Debug.Assert(playerDataContainer != null, "playerDataContainer != null");
            Debug.Assert(playerNameText != null, "playerNameText != null");
            Debug.Assert(playerStateText != null, "playerStateText != null");
        }
    }
}