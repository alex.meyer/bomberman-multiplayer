using UnityEngine;

namespace CommonControllers
{
    public class ColorByTimeController : MonoBehaviour
    {
        [SerializeField] private CountdownTickController countdownTickController;
        [SerializeField] private Renderer materialRenderer;

        [SerializeField] private Color startColor;
        [SerializeField] private Color targetColor;

        private float TimeInSeconds => countdownTickController.TimeInSeconds;

        private void OnEnable()
        {
            ValidateReferences();
            RegisterListeners();
        }

        private void OnDisable()
        {
            UnregisterListeners();
        }

        private void Tick(float timeLeftInSeconds)
        {
            var normalisedTime = (TimeInSeconds - timeLeftInSeconds) / TimeInSeconds;
            materialRenderer.material.color = Color.Lerp(startColor, targetColor, normalisedTime);
        }

        private void RegisterListeners()
        {
            countdownTickController.getTick += Tick;
        }

        private void UnregisterListeners()
        {
            countdownTickController.getTick -= Tick;
        }

        private void ValidateReferences()
        {
            if (countdownTickController == null)
            {
                GetComponent<CountdownTickController>();
            }

            if (materialRenderer == null)
            {
                materialRenderer = GetComponent<Renderer>();
            }

            Debug.Assert(countdownTickController != null, "countdownTickController != null");
            Debug.Assert(materialRenderer != null, "materialRenderer != null");
        }
    }
}