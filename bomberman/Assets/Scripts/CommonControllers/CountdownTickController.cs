using System;
using System.Collections;
using UnityEngine;

namespace CommonControllers
{
    /*
     * independent countdown tick controller
     * listeners can register to the different ticks
     */
    public class CountdownTickController : MonoBehaviour
    {
        [Header("Meta Data")]
        [SerializeField] private float timeInSeconds;
        public float TimeInSeconds
        {
            get => timeInSeconds;
            set => timeInSeconds = value;
        }

        public Action<float> getTick;
        public Action<int> getSecondTick;
        public Action countdownDone;

        private void OnEnable()
        {
            StartCoroutine(CountdownCoroutine());
        }

        private IEnumerator CountdownCoroutine()
        {
            var timeLeft = timeInSeconds;
            var timeLeftInt = (int) timeLeft;
            while (timeLeft > 0f)
            {
                timeLeft -= Time.deltaTime;

                getTick?.Invoke(timeLeft);

                if (timeLeft < timeLeftInt)
                {
                    timeLeftInt = (int) timeLeft;
                    getSecondTick?.Invoke(timeLeftInt);
                }

                if (timeLeft <= 0f)
                {
                    break;
                }

                yield return null;
            }

            countdownDone?.Invoke();
        }
    }
}